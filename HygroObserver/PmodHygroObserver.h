//The MIT License (MIT)
//Copyright (c) 2016-2020 Schneider-Electric
//
//Permission is hereby granted, free of charge,
//to any person obtaining a copy of this software
//and associated documentation files (the "Software"),
//to deal in the Software without restriction,
//including without limitation the rights to use, copy, modify,
//merge, publish, distribute, sublicense, and/or sell copies of the Software,
//and to permit persons to whom the Software is furnished to do so,
//subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included
//in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS",
//WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
//IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
//TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef SFPMODHYGROOBSERVER_H
#define SFPMODHYGROOBSERVER_H

#include "MsgObserverObject.h"
#include "PmodHygroObserverTemperature.h"
#include "PmodHygroObserverHumidity.h"

namespace SF {

class PmodHygroObserver : public MsgObserverObject
{
public:
    PmodHygroObserver(
            const uint8_t& adapter
            ,const timespec& sp, const uint32_t& rc, const size_t& maxsub=0
            ,MsgObserverObject* owner=nullptr, const MO_Label& l="hygro");
    virtual ~PmodHygroObserver();
private:
    PmodHygroObserver(const PmodHygroObserver& orig);
    PmodHygroObserver& operator=(const PmodHygroObserver& orig);

    PmodHygro mod;

    PmodHygroObserverTemperature temperature;
    PmodHygroObserverHumidity humidity;
};

}

#endif /* SFPMODHYGROOBSERVER_H */
