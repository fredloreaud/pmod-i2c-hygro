//The MIT License (MIT)
//Copyright (c) 2016-2020 Schneider-Electric
//
//Permission is hereby granted, free of charge,
//to any person obtaining a copy of this software
//and associated documentation files (the "Software"),
//to deal in the Software without restriction,
//including without limitation the rights to use, copy, modify,
//merge, publish, distribute, sublicense, and/or sell copies of the Software,
//and to permit persons to whom the Software is furnished to do so,
//subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included
//in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS",
//WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
//IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
//TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include "PmodHygro.h"
#include "TimeOut.h"
#include "Debug.h"
#include "SMBusDevice.h"

PMOD::PmodHygro::PmodHygro(const uint8_t& adapter)
    :device(adapter, 0x40)
    ,config(HDC1080_SEQ_ACQ) // backup initialization on chip default value
{
    if(!getConfiguration(config))
    {
        _DERR("failed to initialize configuration");
    }
}

PMOD::PmodHygro::~PmodHygro() {}

bool PMOD::PmodHygro::readConfiguration()
{
    const bool ret = device.readWordSwap(0x02, config);

    if(!ret)
    {
        _DERR("failed read configuration");
    }

    return ret;
}

bool PMOD::PmodHygro::getConfiguration(uint16_t& c)
{
    const bool ret = readConfiguration();
    if(ret)
    {
        c = config;
    }
    return ret;
}

bool PMOD::PmodHygro::writeConfiguration(const uint16_t& c)
{
    const bool ret = device.writeWordSwap(0x02, c);

    if(!ret)
    {
        _DERR("failed write configuration");
    }

    return ret;
}

bool PMOD::PmodHygro::setConfiguration(const uint16_t& c)
{
    bool ret = false;

    if(writeConfiguration(c))
    {
        if(readConfiguration())
        {
            if(config==c)
            {
                ret = true;
            }
            else
            {
                _DERR("failed to update configuration");
            }
        }
    }

    return ret;
}

bool PMOD::PmodHygro::getSequentialAcquisition()
{
    const bool ret = config&HDC1080_SEQ_ACQ;
    return ret;
}

bool PMOD::PmodHygro::setSequentialAcquisition(const bool& m)
{
    bool ret = false;

    if(readConfiguration())
    {
        ret = setConfiguration((config&static_cast<uint16_t>(~HDC1080_SEQ_ACQ))|(m?HDC1080_SEQ_ACQ:0x0000));
    }

    return ret;
}

bool PMOD::PmodHygro::getTemperatureHumidityInt(uint16_t &t, uint16_t &h)
{
    bool ret = false;

    if(!getSequentialAcquisition())
    {
        if(!setSequentialAcquisition(true))
        {
            _DERR("failed to set sequential acquisition mode");
        }
    }

    uint8_t buffer[4];
    if(device.i2cWBReadBuffer(0, &buffer[0], 4, 13)) // write 0 to select temperature register followed by humidity
    {
        t = (buffer[0]*256) + buffer[1];
        h = (buffer[2]*256) + buffer[3];
        ret = true;
    }
    else
    {
        _DERR("read temperature error");
    }

    return ret;
}

bool PMOD::PmodHygro::getTemperatureHumidityFloat(float &t, float &h)
{
    bool ret = false;

    uint16_t it = 0;
    uint16_t ih = 0;
    if(getTemperatureHumidityInt(it, ih))
    {
        t = ((it*165.0)/65536.0)-40.0;
        h = (100.0 * ih) / 65536.0;
        ret = true;
    }

    return ret;
}

bool PMOD::PmodHygro::getTemperatureInt(uint16_t &t)
{
    bool ret = false;

    if(getSequentialAcquisition())
    {
        if(!setSequentialAcquisition(false))
        {
            _DERR("failed to set individual acquisition mode");
        }
    }

    if(device.i2cWBReadWordSwap(0, t, 7)) // write 0 to select temperature register
    {
        ret = true;
    }
    else
    {
        _DERR("read temperature error");
    }

    return ret;
}

bool PMOD::PmodHygro::getTemperatureFloat(float &t)
{
    bool ret = false;

    uint16_t i = 0;
    if(getTemperatureInt(i))
    {
        t = ((i*165.0)/65536.0)-40.0;
        ret = true;
    }

    return ret;
}

bool PMOD::PmodHygro::getHumidityInt(uint16_t &h)
{
    bool ret = false;

    if(getSequentialAcquisition())
    {
        if(!setSequentialAcquisition(false))
        {
            _DERR("failed to set individual acquisition mode");
        }
    }

    if(device.i2cWBReadWordSwap(1, h, 7)) // write 1 to select humidity register
    {
        ret = true;
    }
    else
    {
        _DERR("read humidity error");
    }

    return ret;
}

bool PMOD::PmodHygro::getHumidityFloat(float &h)
{
    bool ret = false;

    uint16_t i = 0;
    if(getHumidityInt(i))
    {
        h = (100.0 * i) / 65536.0;
        ret = true;
    }

    return ret;
}
