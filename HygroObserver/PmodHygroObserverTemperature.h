//The MIT License (MIT)
//Copyright (c) 2016-2020 Schneider-Electric
//
//Permission is hereby granted, free of charge,
//to any person obtaining a copy of this software
//and associated documentation files (the "Software"),
//to deal in the Software without restriction,
//including without limitation the rights to use, copy, modify,
//merge, publish, distribute, sublicense, and/or sell copies of the Software,
//and to permit persons to whom the Software is furnished to do so,
//subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included
//in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS",
//WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
//IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
//TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef SFPMODHYGROOBSERVERTEMPERATURE_H
#define SFPMODHYGROOBSERVERTEMPERATURE_H

#include "MsgObserverInputAttribute_t.h"
#include "PmodHygro.h"

namespace SF {

class PmodHygroObserverTemperature : public MsgObserverInputAttribute_t<float>
{
public:
    PmodHygroObserverTemperature(  const float& v
                                    ,const std::string& l
                                    ,MsgObserverObject& owner
                                    ,const size_t& maxsub
                                    ,const timespec& sp
                                    ,const uint32_t& rc
                                    ,PmodHygro* h);
    virtual ~PmodHygroObserverTemperature();

protected:
    virtual bool read(float& t);

private:
    PmodHygroObserverTemperature(const PmodHygroObserverTemperature& orig);
    PmodHygroObserverTemperature& operator=(PmodHygroObserverTemperature& orig);

    PmodHygro* mod;
};
}

#endif /* SFPMODHYGROOBSERVERTEMPERATURE_H */
