//The MIT License (MIT)
//Copyright (c) 2016-2020 Schneider-Electric
//
//Permission is hereby granted, free of charge,
//to any person obtaining a copy of this software
//and associated documentation files (the "Software"),
//to deal in the Software without restriction,
//including without limitation the rights to use, copy, modify,
//merge, publish, distribute, sublicense, and/or sell copies of the Software,
//and to permit persons to whom the Software is furnished to do so,
//subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included
//in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS",
//WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
//IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
//TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include "PmodHygroObserverTemperature.h"

SF::PmodHygroObserverTemperature::PmodHygroObserverTemperature(
        const float& v
        ,const std::string& l
        ,MsgObserverObject& owner
        ,const size_t& maxsub
        ,const timespec& sp
        ,const uint32_t& rc
        ,PmodHygro* h)
    :MsgObserverInputAttribute_t<float>(v, l, owner, maxsub, sp, rc)
    ,mod(h) {}

SF::PmodHygroObserverTemperature::~PmodHygroObserverTemperature(){}

bool SF::PmodHygroObserverTemperature::read(float& t)
{
    bool ret = false;

    if(mod!=nullptr)
    {
        if(mod->getTemperatureFloat(t))
        {
            ret = true;
        }
    }

    return ret;
}
