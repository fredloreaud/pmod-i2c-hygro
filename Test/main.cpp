#include "SignalHandler.h"
#include "PmodHygro.h"
#include "TimeOut.h"
#include "Debug.h"

#include <cstdlib>
#include <unistd.h> // sleep
#include <iostream> // cout
#include <iomanip> // setw

bool getI2cAdapterDeviceResolution(int argc, char** argv, uint8_t& adapter)
{
    bool ret = true;    
    int32_t opt = -1;
    uint64_t tmp = 0;

    if(argc>1)
    {  
	/*use function getopt to get the arguments with option."hu:p:s:v" indicate 
	that option h,v are the options without arguments while u,p,s are the
	options with arguments*/
	while((opt=getopt(argc,argv,"a:"))!=-1)
	{
	    switch(opt)
	    {
		case 'a':
                    tmp = std::strtoul(optarg, nullptr, 10);
                    if( (tmp!=0) && (tmp<=0xffUL) )
                    {
                        adapter = tmp;
                    }
                    else
                    {
                        ret = false;
                    }
		    break;
		default:
                    ret = false;
                    break;
	    }
	}
    }
    
    if(!ret)
    {
        std::cout<<"Usage:   "<<argv[0]<<" [-option] [argument]"<<std::endl;
        std::cout<<"option:  "<<std::endl;
        std::cout<<"         "<<"-a  I2C adapter [1-255]"<<std::endl;
    }
    
    return ret;
}

int main(int argc, char** argv)
{
    int32_t ret = -1;
    
    OSEF::SignalHandler signal(SIGINT, SIGTERM);

    uint8_t adapter = 1;
    if(getI2cAdapterDeviceResolution(argc, argv, adapter))
    {
        PMOD::PmodHygro pmod(adapter);
        
        float ph = 0.0;
        float ch = 0.0;
        float pt = 0.0;
        float ct = 0.0;

        ret = 0;
        do
        {
            if(pmod.getTemperatureHumidityFloat(ct, ch))
            {
                if(ct!=pt)
                {
                    pt = ct;
                    std::cout<<"t="<<ct<<std::endl;
                }
                
                if(ch!=ph)
                {
                    ph = ch;
                    std::cout<<"            rh="<<ch<<std::endl;
                }
                
                if(!OSEF::sleeps(1)) // avoid over-heating by reading slower than once by second
                {
                    ret = -1;
                }
            }
            else
            {
                ret = -1;
            }
        }while(!signal.signalReceived() && (ret==0));
    }

    return ret;
}
