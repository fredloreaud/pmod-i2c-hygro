//The MIT License (MIT)
//Copyright (c) 2016-2020 Schneider-Electric
//
//Permission is hereby granted, free of charge,
//to any person obtaining a copy of this software
//and associated documentation files (the "Software"),
//to deal in the Software without restriction,
//including without limitation the rights to use, copy, modify,
//merge, publish, distribute, sublicense, and/or sell copies of the Software,
//and to permit persons to whom the Software is furnished to do so,
//subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included
//in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS",
//WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
//IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
//TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
//OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef PMODHYGRO_H
#define PMODHYGRO_H

#include "I2CDevice.h"

// https://reference.digilentinc.com/reference/pmod/pmodhygro/reference-manual
// http://www.ti.com/lit/ds/symlink/hdc1080.pdf
// https://github.com/torvalds/linux/blob/master/drivers/iio/humidity/hdc100x.c
// https://elixir.bootlin.com/linux/latest/source/drivers/iio/humidity/hdc100x.c

namespace PMOD {

const uint16_t HDC1080_SEQ_ACQ = 0x1000;

class PmodHygro ///< Diligent Pmod HYGRO with HDC1080
{
public:
    explicit PmodHygro(const uint8_t& adapter); ///< device [0x40]
    virtual ~PmodHygro();

    bool getTemperatureInt(uint16_t& t); ///< temperature signed integer
    bool getTemperatureFloat(float& t); ///< temperature float

    bool getHumidityInt(uint16_t& h); ///< humidity signed integer
    bool getHumidityFloat(float& h); ///< humidity float

    bool getTemperatureHumidityInt(uint16_t& t, uint16_t& h);
    bool getTemperatureHumidityFloat(float& t, float& h);

private:
    PmodHygro(const PmodHygro& orig);
    PmodHygro& operator=(const PmodHygro& orig);

    bool readConfiguration();
    bool writeConfiguration(const uint16_t& c);

    bool getConfiguration(uint16_t& c);
    bool setConfiguration(const uint16_t& c);

    bool getSequentialAcquisition();
    bool setSequentialAcquisition(const bool& m); ///< 0:individual measurement, 1:temperature+humidity sequence

    OSEF::I2CDevice device;
    uint16_t config;
};

}

#endif /* PMODHYGRO_H */
